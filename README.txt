================================================================================

With "Explorer 8 Mode" can be defined as the Internet Explorer 8 displays eight
pages of an Drupal installation.

Requirements
--------------------------------------------------------------------------------
This module is written for Drupal 6.0+.


Installation
--------------------------------------------------------------------------------
Copy the Explorer 8 Mode module folder to your module directory and then enable
on the admin modules page.

Administration
--------------------------------------------------------------------------------
Administer the module in admin/settings/explorer8_mode.

Informations
--------------------------------------------------------------------------------
http://msdn.microsoft.com/en-us/library/cc288472(VS.85).aspx#compat

Author
--------------------------------------------------------------------------------
Quiptime Group
Siegfried Neumann
www.quiptime.com
quiptime [ @ ] gmail [ dot ]com
